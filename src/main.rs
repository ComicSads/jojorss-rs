use rss::Channel;
use std::collections::HashSet;
use std::error::Error;
use std::fs::{File, OpenOptions};
use std::io::{self, prelude::*, BufRead};

const RSS_FEED: &str = "https://hiwamatanoboru.com/feed/";
const CURRENT_PART: &str = "JoJolion";

fn main() {
	let chapters = match get_chapters() {
		Ok(c) => c,
		Err(e) => panic!("Network error! `{}`", e),
	};
	let lines = match check_logfile() {
		Ok(l) => l,
		Err(e) => panic!("Error!! `{}`", e),
	};
	let mut chapters_read = HashSet::new();
	for line in lines.flatten() {
		chapters_read.insert(line);
	}
	for c in chapters.iter() {
		if !chapters_read.contains(c) {
			append_to_file(desktop_path(), c.to_string());
			append_to_file(logfile_path(), c.to_string());
		}
	}
}

fn check_logfile() -> Result<io::Lines<io::BufReader<File>>, Box<dyn Error>> {
	let path = logfile_path();
	if !std::path::Path::new(&path).exists() {
		OpenOptions::new()
			.write(true)
			.create_new(true)
			.open(&path)?;
	}
	let file = File::open(path)?;
	Ok(io::BufReader::new(file).lines())
}

fn logfile_path() -> String {
	let mut path = dirs_next::data_dir().unwrap();
	path.push("jojorss");
	match path.into_os_string().into_string() {
		Ok(s) => s,
		Err(e) => panic!("Could not convert OsString to String! {:?}", e),
	}
}

fn desktop_path() -> String {
	let mut path = dirs_next::desktop_dir().unwrap();
	path.push(format!("A new {} chapter is out!", CURRENT_PART));
	match path.into_os_string().into_string() {
		Ok(s) => s,
		Err(e) => panic!("Could not convert OsString to String! {:?}", e),
	}
}

fn append_to_file(path: String, to_write: String) {
	let mut file = OpenOptions::new()
		.append(true)
		.create(true)
		.open(path)
		.unwrap();
	if let Err(e) = writeln!(file, "{}", to_write) {
		panic!("Couldn't write to file: {}", e);
	}
}

fn get_chapters() -> Result<Vec<String>, Box<dyn Error>> {
	let channel = read_feed()?;
	let mut v: Vec<String> = Vec::new();

	for i in channel.items() {
		let title = i.title().unwrap_or("Couldn't read title!");

		if title.starts_with(CURRENT_PART) && !title.contains("Volume") {
			v.push(title.to_string());
		}
	}
	Ok(v)
}

fn read_feed() -> Result<Channel, Box<dyn Error>> {
	let content = reqwest::blocking::get(RSS_FEED)?.bytes()?;
	let channel = Channel::read_from(&content[..])?;
	Ok(channel)
}
