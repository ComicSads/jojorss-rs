This crate is only tested on Linux, though there's no reason it shouldn't work on Windows and Mac.  
Currently only checks for new chapters of Jojolion. It may be possible to modify it to work with other manga.  
It will create a file in ~/.local/share/ to keep track of what chapter have been seen in the RSS feed.  
To notify you, it creates a file in your Desktop containing the new chapter number named "A new Jojolion chapter is out!"  
How you can run this is up to you, I have it set to run once a day in a cron job.  
This crate relies on dirs-next to manage XDG environment variables.  
